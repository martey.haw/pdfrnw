"""
ASGI config for pdfrnw project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

try:
    from dotenv import load_dotenv

    load_dotenv()
except Exception:
    pass

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pdfrnw.settings")

application = get_asgi_application()
