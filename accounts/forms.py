from django import forms
from django.contrib.auth import get_user_model


class LoginForm(forms.Form):
    username = forms.CharField(
        max_length=254,
        label="E-mail address",
        widget=forms.EmailInput(
            attrs={"class": "input"},
        ),
    )
    password = forms.CharField(
        max_length=128,
        widget=forms.PasswordInput(attrs={"class": "input"}),
    )


class SignUpForm(forms.Form):
    username = forms.CharField(
        max_length=254,
        label="E-mail address",
        widget=forms.EmailInput(
            attrs={"class": "input"},
        ),
    )
    full_name = forms.CharField(
        max_length=200,
        label="Full name",
        widget=forms.TextInput(
            attrs={"class": "input"},
        ),
    )
    short_name = forms.CharField(
        max_length=50,
        label="Preferred name",
        widget=forms.TextInput(
            attrs={"class": "input"},
        ),
    )
    password = forms.CharField(
        max_length=128,
        label="Password",
        widget=forms.PasswordInput(
            attrs={"class": "input"},
        ),
    )
    password_confirmation = forms.CharField(
        max_length=128,
        label="Password confirmation",
        widget=forms.PasswordInput(
            attrs={"class": "input"},
        ),
    )


class EditUserForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = [
            "full_name",
            "short_name",
            "email",
        ]
