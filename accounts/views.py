from django.shortcuts import render, redirect
from django.contrib.auth import (
    login,
    authenticate,
    logout,
    get_user_model,
)
from accounts.forms import LoginForm, SignUpForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_str
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.conf import settings
from django.db import IntegrityError
from django.contrib import messages
from django.template import loader


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user_exists = get_user_model().objects.filter(email=username).exists()
            authenticated_user = authenticate(
                request,
                username=username,
                password=password,
            )
            if authenticated_user:
                login(request, authenticated_user)
                messages.success(request, "Successfully logged in!")
                return redirect("home")
            elif user_exists:
                form.add_error("password", "Incorrect password...")
            else:
                form.add_error("username", "Invalid username...")
    else:
        form = LoginForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    messages.success(request, "Successfully logged out")
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            full_name = form.cleaned_data["full_name"]
            short_name = form.cleaned_data["short_name"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                try:
                    user = get_user_model().objects.create_user(
                        username,
                        password=password,
                        full_name=full_name,
                        short_name=short_name,
                        is_active=False,
                    )

                    current_site = get_current_site(request)
                    mail_subject = "Activate your account"
                    template = loader.get_template("confirm_email.txt")
                    template_context = {
                        "user": user,
                        "domain": current_site.domain,
                        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                        "token": account_activation_token.make_token(user),
                    }
                    message = template.render(template_context)
                    email = EmailMessage(mail_subject, message, from_email=settings.EMAIL_HOST_USER, to=[username])
                    email.content_subtype = "html"
                    email.send()
                    messages.success(request, "An email was sent to confirm your registration.")
                    return redirect("login")
                except IntegrityError:
                    form.add_error("username", "That email is already taken...")
            else:
                form.add_error("password", "The passwords do not match...")
                form.add_error("password_confirmation", " ")
    else:
        form = SignUpForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)


def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = get_user_model().objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, get_user_model().DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        messages.success(request, "Thank you for your email confirmation. You are now logged in.")
        return redirect("home")
    else:
        messages.error(request, "Activation link is invalid!")
        return redirect("login")
