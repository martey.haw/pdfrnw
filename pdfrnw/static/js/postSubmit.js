function disableButton(buttonId) {
	button = document.getElementById(buttonId);
	button.disabled = true;
	button.classList.add('is-loading');
}
