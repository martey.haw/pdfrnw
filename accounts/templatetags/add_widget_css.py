from django import template

register = template.Library()


@register.filter
def add_widget_css(value, arg):
    css_classes = value.field.widget.attrs.get("class", "").split(" ")
    if css_classes and arg not in css_classes:
        css_classes = f"{' '.join(css_classes)} {arg}"
    return value.as_widget(attrs={"class": css_classes})
